<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT COUNT(*)total FROM ciclista',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*)total FROM ciclista",
            
        ]);
    }
    
    public function actionConsulta1a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("COUNT(*)total")->distinct(),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*)total FROM ciclista",
            
        ]);
    }
    
    public function actionConsulta2 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT COUNT(*)total FROM ciclista WHERE nomequipo = "banesto"',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*)total FROM ciclista WHERE nomequipo = 'banesto'",
            
        ]);
    }
    
    public function actionConsulta2a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("COUNT(*)total")->distinct()->where("nomequipo = 'banesto'"),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*)total FROM ciclista WHERE nomequipo = 'banesto'",
            
        ]);
    }
    
    public function actionConsulta3 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT AVG(edad)media FROM ciclista',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad)media FROM ciclista",
            
        ]);
    }
    
    public function actionConsulta3a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("AVG(edad)media")->distinct(),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad)media FROM ciclista",
            
        ]);
    }
    
    public function actionConsulta4 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT AVG(edad)media FROM ciclista WHERE nomequipo = "banesto"',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad)media FROM ciclista WHERE nomequipo = 'banesto'",
            
        ]);
    }
    
    public function actionConsulta4a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("AVG(edad)media")->distinct()->where("nomequipo = 'banesto'"),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad)media FROM ciclista WHERE nomequipo = 'banesto'",
            
        ]);
    }
    
    public function actionConsulta5 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT AVG(edad)media FROM ciclista GROUP BY nomequipo',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad)media FROM ciclista GROUP BY nomequipo",
            
        ]);
    }
    
    public function actionConsulta5a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("AVG(edad)media,nomequipo")->groupBy("nomequipo")
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad)media FROM ciclista GROUP BY nomequipo",
            
        ]);
    }
    
    public function actionConsulta6 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT COUNT(*)numero, nomequipo FROM ciclista GROUP BY nomequipo',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['numero','nomequipo'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(*)numero, nomequipo FROM ciclista GROUP BY nomequipo",
            
        ]);
    }
    
    public function actionConsulta6a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("COUNT(*)numero,nomequipo")->groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['numero','nomequipo'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(*)numero, nomequipo FROM ciclista GROUP BY nomequipo",
            
        ]);
    }
    
    public function actionConsulta7 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT COUNT(*)total FROM puerto',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*)total FROM puerto",
            
        ]);
    }
    
    public function actionConsulta7a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => \app\models\Puerto::find()->select("COUNT(*)total")->distinct(),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*)media FROM puerto",
            
        ]);
    }
    
    public function actionConsulta8 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT COUNT(*)numero FROM puerto WHERE altura > 1500',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*)numero FROM puerto WHERE altura > 1500",
            
        ]);
    }
    
    public function actionConsulta8a()
    {
        $dataProvider = new ActiveDataProvider([
         'query' => \app\models\Puerto::find()->select("COUNT(*)numero")->distinct()->where("altura > 1500"),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*)numero FROM puerto WHERE altura > 1500",
            
        ]);
    }
    
    public function actionConsulta9 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>' SELECT distinct COUNT(*)total,  nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal) > 4',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT COUNT(*)total,  nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(*) > 4",
            
        ]);
    }
    
    public function actionConsulta9a()
    {
       
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("COUNT(*) total,nomequipo")->groupBy('nomequipo')->having("COUNT(dorsal) > 4"),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT COUNT(*)total,  nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(*) > 4",
            
        ]);
    }
    
     public function actionConsulta10 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>' SELECT COUNT(*), edad, nomequipo FROM  ciclista '
            . 'GROUP BY  nomequipo HAVING COUNT(*) > 5 AND edad BETWEEN 28 AND 32',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT COUNT(*), edad, nomequipo FROM  ciclista  
                    GROUP BY  nomequipo HAVING COUNT(*) > 4 AND edad BETWEEN 28 AND 32",
            
        ]);
    }
    
    public function actionConsulta10a()
    {
       
        $dataProvider = new ActiveDataProvider([
         'query' => Ciclista::find()->select("COUNT(*), edad, nomequipo")->groupBy('nomequipo')->having("COUNT(*) > 4 AND edad BETWEEN 28 AND 32"),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT COUNT(*)total,  nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(*) > 4 AND edad BETWEEN 28 AND 32",
            
        ]);
    }
    
    
    public function actionConsulta11 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>' SELECT DISTINCT dorsal, COUNT(*) numero FROM etapa GROUP BY dorsal ',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(*) numero FROM etapa GROUP BY dorsal ",
            
        ]);
    }
    
    public function actionConsulta11a()
    {
       
        $dataProvider = new ActiveDataProvider([
         'query' => \app\models\Etapa::find()->select("dorsal, COUNT(*) numero")->groupBy('dorsal'),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(*) numero FROM etapa GROUP BY dorsal ",
            
        ]);
    }
    
     public function actionConsulta12 ()
    {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>' SELECT DISTINCT dorsal, COUNT(*)a FROM etapa GROUP BY dorsal HAVING a > 1 ',
            
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(*)a FROM etapa GROUP BY dorsal HAVING a > 1 ",
            
        ]);
    }
    
    public function actionConsulta12a()
    {
       
        $dataProvider = new ActiveDataProvider([
         'query' => \app\models\Etapa::find()->select("dorsal, COUNT(*)a")->groupBy('dorsal')->having('a > 1'),
        ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(*)a FROM etapa GROUP BY dorsal HAVING a > 1 ",
            
        ]);
    }
    
}
